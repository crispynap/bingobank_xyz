function Commune(name){
	this.name = name;
	this.people = [];
	this.addPerson = function(name){
		this.people.push(name)
	}
	this.printPerson = function(){
		this.people.forEach(function(v){
			v.print();
		})
	}
}

function Person(name){
	this.name = name;
	this.setAttr = function(attr){
		this.attr = attr;
	}
	this.print = function(){
		console.log(this.attr);
	}
}

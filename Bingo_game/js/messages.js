$('head').data('start_000', {
	'empty': true,
	'text': '빈고 게임에 오신 것을 환영합니다.',
	'next_button': true,
	'next_message': 'start_001'
})

$('head').data('start_001', {
	'empty': true,
	'text': '이 게임에서 당신은 공동체은행 빈고의 활동가가 됩니다.',
	'next_button': true,
	'next_message': 'start_002'
})

$('head').data('start_002', {
	'text': '이 게임을 통해 공동체은행이 어떻게 운영되는 지 직접 겪어볼 것입니다.',
	'next_message': 'start_003'
})

$('head').data('start_003', {
	'empty': true,
	'text': '동시에, 당신은 헬조선을 살아가는 한 명의 생활인이기도 합니다.',
	'next_button': true,
	'next_message': 'start_004'
})

$('head').data('start_004', {
	'text': '이 게임을 통해 활동가의 삶이 어떻게 시궁창이 되는지 알 수 있을',
	'start_scene': true,
	'next_scene': 'start_004_1',
	'delay': '2000'
})

$('head').data('start_005', {
	'text': '',
	'next_message': 'start_006'
})

$('head').data('start_006', {
	'empty': true,
	'text': '로그인 여부를 확인합니다.',
	'check_login': true
})

$('head').data('start_007', {
	'empty': true,
	'text': '환영합니다, 정민님. 게임을 시작합시다.',
	'next_message': 'start_008'
})

$('head').data('start_008', {
	'empty': true,
	'text': '2008년 2월 21일, 빈집이 만들어졌습니다.',
	'make_time': true,
	'next_message': 'start_009'
})

$('head').data('start_009', {
	'text': '빈집은 복잡한 곳이지만, 이 게임에서는 빈집에 대해 두 가지만 알면 됩니다.',
	'next_message': 'start_010'
})


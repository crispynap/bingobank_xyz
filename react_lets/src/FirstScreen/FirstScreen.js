import React from "react";
import { StatusBar, Image, View, StyleSheet } from "react-native";
import { Container, Header, Title, Left, Icon, Right, Button, Body, Content, Text, Card, CardItem } from "native-base";
import { Col, Row, Grid } from 'react-native-easy-grid';
export default class HomeScreen extends React.Component {
	constructor(...args) {
		super(...args);
		this.test = this.test.bind(this);
	}

	test() {
		console.log('456');
		this.props.navigation.navigate("Profile")
	}

	render() {
		console.log('123');

		return (
			<Container style={styles.container}>
				<View style={styles.upside}>
					<View style={styles.row}>
						<Image
							source={require('../../assets/image/logo.png')}
							resizeMode='contain'
							style={{ width: 350, height: 350, alignSelf: 'center', }}
						>
						</Image>
						<Text style={styles.bigText}>
							LETS APP
					</Text>
						<Text style={styles.smallText}>
							(ver.Alpha)
						</Text>
					</View>
				</View>
				<View style={styles.downside}>
					<Button block large iconLeft
						style={styles.googleButton}
						//onPress={() => this.props.navigation.navigate("Profile")}
						onPress={this.test}
					>
						<Icon name='logo-googleplus' />
						<Text style={{ fontSize: 20, fontFamily: "Entypo" }}>구글 계정으로 로그인</Text>
					</Button>
				</View>
			</Container >
		);
	}
}
const styles = StyleSheet.create({
	container: {
		flex: 1,
		flexDirection: 'column',
		backgroundColor: '#2C6EAD'
	},
	googleButton:
	{
		margin: 10,
		marginTop: 20,
		backgroundColor: '#0D448F',
	},
	bigText: {
		marginTop: 10,
		fontFamily: 'Entypo',
		color: '#E6BE50',
		fontWeight: 'bold',
		alignSelf: 'center',
		fontSize: 65
	},
	smallText: {
		marginTop: -15,
		textAlign: 'right',
		fontFamily: 'PingFangTC-Semibold',
		color: '#E6BE50',
		fontWeight: 'bold',
		fontSize: 20
	},
	row: {
		flex: 1,
		// backgroundColor: 'red'
	},
	upside: {
		flex: 3,
		flexDirection: 'row',
		alignItems: 'center',
		// backgroundColor: 'red'
	},
	downside: {
		flex: 1,
		alignItems: 'center',
		// backgroundColor: 'green'
	}
});


import {
	AppRegistry
} from 'react-native';

import app from './app';

AppRegistry.registerComponent('react_lets', () => app);

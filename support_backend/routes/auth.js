module.exports = function (passport) {
	var route = require('express').Router();
	var conn = require('../config/db')();
	var bkfd2Password = require('pbkdf2-password');
	var hasher = bkfd2Password();
	var auth = require('../auth');

	route.post('/login',
		// function (req, res) {
		// 	console.log(req.query);
		// }

		passport.authenticate('local'),
		function (req, res) {
			console.log(req.query)
			res.redirect('/topic')
		}
	);

	route.post('/register', function (req, res) {
		console.log(req)

		hasher({
			password: req.query.password
		}, function (err, pass, salt, hash) {
			var user = {
				authId: 'local:' + req.query.userId,
				userId: req.query.userId,
				password: hash,
				salt: salt,
				nickname: req.query.nickname,
				email: req.query.email
			};
			var sql = 'INSERT INTO users SET ?';
			conn.query(sql, user, function (err, results) {
				if (err) {
					console.log(err);
					res.status(500);
				} else {
					req.login(user, function (err) {
						req.session.save(function () {
							res.redirect('/topic');
						});
					});
				}
			});
		});
	});
	route.get('/register', function (req, res) {
	});
	// route.get('/login', function (req, res) {
	// 	res.render('auth/login');
	// });

	// route.get('/logout', function (req, res) {
	// 	req.logout();
	// 	req.session.save(function () {
	// 		res.redirect('/topic');
	// 	});
	// });

	route.post('/register/idcheck', function (req, res) {
		var sql = 'SELECT userId FROM users WHERE userId = ?';
		conn.query(sql, req.body.data, function (err, results) {
			if (err) {
				console.log(err);
				res.status(500);
			} else {
				if (results[0]) {
					res.send('found');
				} else {
					res.send('not found');
				}
			}
		})
	});

	route.post('/register/nickcheck', function (req, res) {
		var sql = 'SELECT nickname FROM users WHERE nickname = ?';
		conn.query(sql, req.body.data, function (err, results) {
			if (err) {
				console.log(err);
				res.status(500);
			} else {
				if (results[0]) {
					res.send('found');
				} else {
					res.send('not found')
				}
			}
		})
	});


	route.get('/google',
		passport.authenticate('google', { scope: ['openid', 'email'] }),
		function (req, res) {
			// The request will be redirected to Google for authentication, so this
			// function will not be called.
		});

	route.get('/google/callback',
		passport.authenticate('google', { failureRedirect: '/auth/login' }),
		function (req, res) {
			console.log(req.query);
			res.redirect('/');
		});

	route.get('/logout', function (req, res) {
		req.logout();
		res.redirect('/');
	});

	route.get('/login', function (req, res, next) {
		res.render('auth/login', { title: 'Login' });
	});

	route.get('/account', auth.ensureAuthenticated, function (req, res, next) {
		res.render('auth/account', {
			title: 'Account',
			name: req.user.displayName,
			user: JSON.stringify(req.user)
		});
	});


	return route;
};
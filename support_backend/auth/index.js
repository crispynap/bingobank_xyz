'use strict';

var ensureAuthenticated = function (req, res, next) {
	if (req.isAuthenticated()) { return next(); }
	res.redirect('/auth/login');
};

exports.ensureAuthenticated = ensureAuthenticated;
module.exports = function (app) {
	var conn = require('./db')();
	var bkfd2Password = require('pbkdf2-password');
	var passport = require('passport')
		, LocalStrategy = require('passport-local').Strategy
		, GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
	var google = require('./google.json');

	var hasher = bkfd2Password();
	app.use(passport.initialize());
	app.use(passport.session());

	var GOOGLE_CLIENT_ID = google.web.client_id;
	var GOOGLE_CLIENT_SECRET = google.web.client_secret;

	// passport.serializeUser(function (user, done) {
	// 	done(null, user.authId);
	// });

	// passport.deserializeUser(function (id, done) {
	// 	var sql = 'SELECT * FROM users WHERE authId=?';
	// 	conn.query(sql, [id], function (err, results) {
	// 		if (err) {
	// 			console.log(err);
	// 			done('There is no user.');
	// 		}
	// 		else {
	// 			done(null, results[0]);
	// 		}
	// 	})
	// });
	passport.serializeUser(function (user, done) {
		done(null, user);
	});

	passport.deserializeUser(function (obj, done) {
		done(null, obj);
	});

	passport.use(new LocalStrategy(
		function (username, password, done) {
			var uname = username;
			var pwd = password;
			var sql = 'SELECT * FROM users WHERE authId=?';
			conn.query(sql, ['local:' + uname], function (err, results) {
				if (err) {
					return done('There is no user.');
				}
				var user = results[0];
				return hasher({ password: pwd, salt: user.salt }, function (err, pass, salt, hash) {
					if (hash === user.password) {
						return done(null, user);
					} else {
						return done(null, false, { message: '비번 틀림' });
					}
					return done(null, false, { message: '아이디 없음' });
				});
			})
		}
	));


	passport.use(new GoogleStrategy({
		clientID: GOOGLE_CLIENT_ID,
		clientSecret: GOOGLE_CLIENT_SECRET,
		callbackURL: "http://bingobank.xyz:3000/auth/google/callback"
	},
		function (accessToken, refreshToken, profile, done) {
			// asynchronous verification, for effect...
			process.nextTick(function () {

				// To keep the example simple, the user's Google profile is returned to
				// represent the logged-in user.  In a typical application, you would want
				// to associate the Google account with a user record in your database,
				// and return that user instead.
				return done(null, profile);
			});
		}
	));

	return passport;
}


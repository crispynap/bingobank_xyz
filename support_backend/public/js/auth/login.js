/**********************************/
//로그인 버튼을 눌렀을 경우
//조건을 전부 검사해서 문제 있는 조건 입력칸으로 이동
$('#buttonLogin').click(() => {
	Promise.all([idCheck(), passwordCheck()])
		.then(() => {
			$('#loginForm').submit();
		})
		.catch(err => {
			console.log(err)
			if (err === 'wrong id' || err === 'same id found' || err === 'short id' || err === 'long id') {
				$("#inputId").focus();
			} else if (err === 'short pwd') {
				$("#inputPassword").focus();
			} else if (err === 'wrong pwd') {
				$("#inputPasswordCheck").focus().select();
			} else if (err === 'same nick found' || err === 'insert nick') {
				$("#inputNickname").focus();
			} else if (err === 'wrong Email') {
				$("#inputEmail").focus();
			}
		})
})
/**********************************/
//id 존재 여부 검사
var idCheck = () => {
	return new Promise((resolve, reject) => {
		var userId = $("#inputId").val();
		$.ajax({
			url: '/auth/register/idcheck',
			type: 'post',
			dataType: 'text',
			error: (request, status, error) => {
				reject(error);
			},
			data: "data=" + userId,
			success: (data) => {
				if (data == 'found') {
					reject('same id found');
				} else {
					resolve();
				}
			}
		})
	});
}
